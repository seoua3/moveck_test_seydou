// dear imgui: standalone example application for GLFW + OpenGL 3, using programmable pipeline
// If you are new to dear imgui, see examples/README.txt and documentation at the top of imgui.cpp.
// (GLFW is a cross-platform general purpose library for handling windows, inputs, OpenGL/Vulkan graphics context creation, etc.)

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include "ctype.h"

// fibonacci array length
#define FIB_ARRAY_LENGTH 6 // size of the array when creating a fibonacci sequence

// global variable
std::vector<int> read_array; // array containing fibonacci's sequence
char start_option; 

// About Desktop OpenGL function loaders:
//  Modern desktop OpenGL doesn't have a standard portable header file to load OpenGL function pointers.
//  Helper libraries are often used for this purpose! Here we are supporting a few common ones (gl3w, glew, glad).
//  You may use another loader/header of your choice (glext, glLoadGen, etc.), or chose to manually implement your own.
#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
#include <GL/gl3w.h>    // Initialize with gl3wInit()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
#include <GL/glew.h>    // Initialize with glewInit()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
#include <glad/glad.h>  // Initialize with gladLoadGL()
#else
#include IMGUI_IMPL_OPENGL_LOADER_CUSTOM
#endif

// Include glfw3.h after our OpenGL definitions
#include <GLFW/glfw3.h>

// [Win32] Our example includes a copy of glfw3.lib pre-compiled with VS2010 to maximize ease of testing and compatibility with old VS compilers.
// To link with VS2010-era libraries, VS2015+ requires linking with legacy_stdio_definitions.lib, which we do using this pragma.
// Your own project should not be affected, as you are likely to link with a newer binary of GLFW that is adequate for your version of Visual Studio.
#if defined(_MSC_VER) && (_MSC_VER >= 1900) && !defined(IMGUI_DISABLE_WIN32_FUNCTIONS)
#pragma comment(lib, "legacy_stdio_definitions")
#endif

static void glfw_error_callback(int error, const char* description)
{
    fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}


// Creates a fibonacci sequence of length FIB_ARRAY_LENGTH (see top of the file value)
void create_fibonacci_suite(int first_number, int second_number)
{
    int next_number = first_number + second_number;

    read_array.push_back(first_number);
    read_array.push_back(second_number);
    read_array.push_back(next_number);

    for (int i = 3; i < FIB_ARRAY_LENGTH; i++)
    {
        first_number = second_number;
        second_number = next_number;
        next_number = first_number + second_number;
        read_array.push_back(next_number);
    }

}


// Writes sequence to a file
// returns 0 if successful
// returns < 0 if error
int write_to_file()
{
    std::ofstream saveSuiteFile;
    saveSuiteFile.open("suites.txt", std::ios::app);
    if (!saveSuiteFile)
    {
        std::cout << std::endl << "ERREUR LORS DE L'OUVERTURE DU FICHIER (write_to_file function)" << std::endl;
        return -1;
    }

    for (int i = 0; i < FIB_ARRAY_LENGTH; i++)
    {
        saveSuiteFile << read_array[i] << ","; // separates number by a comma
    }
    saveSuiteFile << "." << std::endl;
    saveSuiteFile.close();
    return 0;
}


// Reads a sequence from a file and puts it in the global variable read_array
// returns 0 if succesful
// returns < 0 if error
int read_from_file()
{
    std::string abc;
    std::string fileName = ".\\suites.txt";
    std::ifstream readSuiteFile;

    readSuiteFile.open(fileName);

    if (!readSuiteFile)
    {
        std::cout << std::endl << "ERREUR LORS DE L'OUVERTURE DU FICHIER (read_from_file function)" << std::endl;
        return -1;
    }

    while (std::getline(readSuiteFile, abc, ','))
    {
        read_array.push_back(std::stoi(abc)); 
        if (readSuiteFile.peek() == '.')
        {
            break;
        }
    }

    readSuiteFile.close();

    return 0;
}


// Approximate golden spiral using bezier curves
void DrawFibonacciCurve(ImDrawList* draw_list, ImVec2 pMin, ImVec2 pMax, int direction)
{
    ImVec2 realP1 = pMin;
    ImVec2 realP2 = pMax;

    float rectLenghtX = pMax.x - pMin.x;
    float rectLenghtY = pMax.y - pMin.y;
    
    float curveStrength = 1.75f;

    //case 2 is the default
    ImVec2 curveP1 = ImVec2(realP1.x + rectLenghtX / curveStrength, realP1.y);
    ImVec2 curveP2 = ImVec2(realP2.x, realP2.y - rectLenghtY / curveStrength);


    switch (direction) 
    {
        case 1:
            realP1 = ImVec2(pMin.x, pMax.y);
            realP2 = ImVec2(pMax.x, pMin.y);

            curveP1 = ImVec2(realP1.x, realP1.y - rectLenghtY / curveStrength);
            curveP2 = ImVec2(realP1.x + rectLenghtX / curveStrength, realP2.y);
            break;
        case 3:
            realP1 = ImVec2(pMax.x, pMin.y);
            realP2 = ImVec2(pMin.x, pMax.y);

            curveP1 = ImVec2(realP1.x, realP1.y + rectLenghtY / curveStrength);
            curveP2 = ImVec2(realP2.x + rectLenghtX / curveStrength, realP2.y);
            break;
        case 4:
            realP1 = ImVec2(pMax);
            realP2 = ImVec2(pMin);

            curveP1 = ImVec2(realP1.x - rectLenghtX / curveStrength, realP1.y);
            curveP2 = ImVec2(realP2.x, realP2.y + rectLenghtY / curveStrength);
            break;
    }



    draw_list->AddBezierCurve(realP1, curveP1, curveP2, realP2, IM_COL32(0, 255, 255, 255), 1.0f, 0);
}


int main(int, char**)
{
    std::cout << "Desirez-vous creer une suite et la sauvegarder dans un fichier ? ( w )"
              << "\nou desirez-vous charger une suite deja creee dans le fichier suites.txt ? ( c )"
              << std::endl << std::endl;

    std::cin >> start_option;

    while (start_option != 'w' && start_option != 'c')
    {
        std::cout << "Veuillez rentrer une option valide." << std::endl
                  << "'w' pour creer une suite et la sauvegarder dans un fichier," << std::endl
                  << "'c' pour charger une suite du fichier suites.txt" << std::endl;

        std::cin >> start_option;
    }

    if (start_option == 'w')
    {
        // Asking numbers from the user
        std::cout << "Veuillez rentrer le premier nombre de la suite" << std::endl;
        int first = 0;
        std::cin >> first;

        //while (isdigit(first)) // validating the first number ******************************* a completer
        //{
        //    std::cout << "Veuillez rentrer une nombre valide" << std::endl;
        //    std::cin >> first;
        //}

        std::cout << "Veuillez rentrer le deuxieme nombre de la suite" << std::endl;
        int second;
        std::cin >> second;

        //while (isdigit(second)) // validating the second number ******************************** a completer
        //{
        //    std::cout << "Veuillez rentrer un nombre valide " << std::endl;
        //    std::cin >> second;
        //}

        // Creating fibonacci sequence array
        create_fibonacci_suite(first, second);

    
        // Writing suite in file
        if (write_to_file())
        {
            std::cout << std::endl << "ERREUR LORS DE L'ECRITURE AU FICHIER" << std::endl;
        }
    }

    else 
    {
        if (start_option == 'c')
        {
            // Reading suite from existing file
            std::string file = "suites.txt";
            if (read_from_file())
            {
                std::cout << std::endl << "ERREUR LORS DE LA LECTURE DU FICHIER" << std::endl;
            }
        }
    }

    

    
    //********************************************
    //                  TO DO                   //
    //********************************************
    // refactor main(), remove windows we dont need, add those we need
    // thread for window, thread for rest
    

    // Setup window
    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit())
        return 1;

    // Decide GL+GLSL versions
#if __APPLE__
    // GL 3.2 + GLSL 150
    const char* glsl_version = "#version 150";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // Required on Mac
#else
    // GL 3.0 + GLSL 130
    const char* glsl_version = "#version 130";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
    //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // 3.0+ only
#endif

    // Create window with graphics context
    GLFWwindow* window = glfwCreateWindow(1280, 720, "Dear ImGui GLFW+OpenGL3 example", NULL, NULL);
    if (window == NULL)
        return 1;
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1); // Enable vsync

    // Initialize OpenGL loader
#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
    bool err = gl3wInit() != 0;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
    bool err = glewInit() != GLEW_OK;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
    bool err = gladLoadGL() == 0;
#else
    bool err = false; // If you use IMGUI_IMPL_OPENGL_LOADER_CUSTOM, your loader is likely to requires some form of initialization.
#endif
    if (err)
    {
        fprintf(stderr, "Failed to initialize OpenGL loader!\n");
        return 1;
    }

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    // Setup Platform/Renderer bindings
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);

    // Our state
    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
   

    // Main loop
    while (!glfwWindowShouldClose(window))
    {
        // Poll and handle events (inputs, window resize, etc.)
        // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
        // - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
        // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
        // Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
        glfwPollEvents();

        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        // Window size and position
        ImGui::SetNextWindowSize(io.DisplaySize);
        ImGui::SetNextWindowPos(ImVec2(0, 0));
        
        if (!ImGui::Begin("Fibonacci spiral",NULL, ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_AlwaysHorizontalScrollbar | ImGuiWindowFlags_AlwaysVerticalScrollbar))
        {
            ImGui::End();
            return 0;
        }
        

        // Tip: If you do a lot of custom rendering, you probably want to use your own geometrical types and benefit of overloaded operators, etc.
        // Define IM_VEC2_CLASS_EXTRA in imconfig.h to create implicit conversions between your types and ImVec2/ImVec4.
        // ImGui defines overloaded operators but they are internal to imgui.cpp and not exposed outside (to avoid messing with your types)
        // In this example we are not using the maths operators!
        ImDrawList* draw_list = ImGui::GetWindowDrawList();
        
       
        static float square_side_length = 15.0f;
        float sq_len = square_side_length;
        static float square_thickness = 1.0f;
        static ImVec4 colf = ImVec4(4.0f, 1.0f, 2.0f, 3.0f);
        const ImU32 color = ImColor(colf);
        static float x_start = ImGui::GetWindowWidth() / 2.0f;
        static float y_start = ImGui::GetWindowHeight() / 2.0f;
        const ImVec2 p = ImVec2(x_start, y_start);
        float x = p.x + 4.0f, y = p.y + 4.0f;
        //float spacing = 10.0f;
        ImDrawCornerFlags corners_none = 0;

        int cnt = 0;
        int n = 1;
        int iter = 0;

        while (true)
        {
            for (n = 1; n <= 4; n++)
            {
                if (n == 2)
                {
                    x += read_array[n - 2 + iter] * sq_len;
                }
                if (n == 3)
                {
                    x -= read_array[n - 3 + iter] * sq_len;
                    y -= read_array[n - 2 + iter] * sq_len;
                }
                if (n == 4)
                {
                    x -= read_array[n - 1 + iter] * sq_len;
                    y += read_array[n - 3 + iter] * sq_len;
                }

                ImVec2 squareMin = ImVec2(x, y);
                ImVec2 squareMax = ImVec2(x + read_array[n - 1 + iter] * sq_len, y - read_array[n - 1 + iter] * sq_len);

                draw_list->AddRect(
                    squareMin,
                    squareMax,
                    color,
                    0.0f,
                    corners_none,
                    square_thickness
                );
                                   
                
           
                DrawFibonacciCurve(draw_list, squareMin, squareMax, n);
           
                cnt++;

                if (cnt == read_array.size())
                {
                    break;
                }

            }

            //x += read_array[4] * sq_len;
            if (cnt == read_array.size())
            {
                break;
            }
            y += read_array[4 + iter] * sq_len;
            iter += 4;
        }

        

        ImGui::End();

        // Rendering
        ImGui::Render();
        int display_w, display_h;
        glfwGetFramebufferSize(window, &display_w, &display_h);
        gl3wViewport(0, 0, display_w, display_h);
        glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
        glClear(GL_COLOR_BUFFER_BIT);
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        glfwSwapBuffers(window);
    }

    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}