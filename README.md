Lorsque l'on lance le programme, deux choix s'offrent à nous :
1. L'option 'w' qui crée une suite de fibonacci de longueur "FIB_ARRAY_LENGTH" (défini dans le haut du main.cpp) 
   ,la sauvegarde dans le fichier suites.txt (dans le dossier DEBUG) et l'affiche dans une fenêtre.
2. L'option 'c' qui va prendre la première suite du fichier suites.txt et l'afficher dans une fenêtre.

